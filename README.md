NRG Aggregator Library (nrg-all)
================================

The NRG aggregator library provides an easy way to build and deploy all of the
standard NRG framework libraries in a single operation. This aggregates all of
the upstream modules into a single build list.

Contents
--------

The referenced libraries are:

-   NRG parent (https://bitbucket.org/nrg/nrg\_parent)

-   NRG Framework (https://bitbucket.org/nrg/nrg\_framework)

-   NRG Transaction (https://bitbucket.org/nrg/nrg\_transaction)

-   NRG Prefs (https://bitbucket.org/nrg/nrg\_prefs)

-   NRG Configuration Service (https://bitbucket.org/nrg/nrg\_config)

-   NRG Automation (https://bitbucket.org/nrg/nrg\_automation)

-   NRG DICOM Tools (https://bitbucket.org/nrg/nrg\_dicomtools)

-   NRG Anonymize (https://bitbucket.org/nrg/nrg\_anonymize)

-   NRG Mail (https://bitbucket.org/nrg/nrg\_mail)

-   NRG Notify (https://bitbucket.org/nrg/nrg\_notify)

Building
--------

To build all of these modules, invoke Maven with the desired lifecycle phase.
For example, the following command will clean previous builds then build new 
jar files, along with archives containing each library's source code and JavaDocs,
run each library's unit tests, and install each jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~